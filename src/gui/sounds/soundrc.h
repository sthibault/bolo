#ifndef _SOUNDRC
#define _SOUNDRC
extern const unsigned char big_explosion_far_wav[];
extern const unsigned big_explosion_far_wav_size;
extern const unsigned char big_explosion_near_wav[];
extern const unsigned big_explosion_near_wav_size;
extern const unsigned char bubbles_wav[];
extern const unsigned bubbles_wav_size;
extern const unsigned char farming_tree_far_wav[];
extern const unsigned farming_tree_far_wav_size;
extern const unsigned char farming_tree_near_wav[];
extern const unsigned farming_tree_near_wav_size;
extern const unsigned char hit_tank_far_wav[];
extern const unsigned hit_tank_far_wav_size;
extern const unsigned char hit_tank_near_wav[];
extern const unsigned hit_tank_near_wav_size;
extern const unsigned char hit_tank_self_wav[];
extern const unsigned hit_tank_self_wav_size;
extern const unsigned char man_building_far_wav[];
extern const unsigned man_building_far_wav_size;
extern const unsigned char man_building_near_wav[];
extern const unsigned man_building_near_wav_size;
extern const unsigned char man_dying_far_wav[];
extern const unsigned man_dying_far_wav_size;
extern const unsigned char man_dying_near_wav[];
extern const unsigned man_dying_near_wav_size;
extern const unsigned char man_lay_mine_near_wav[];
extern const unsigned man_lay_mine_near_wav_size;
extern const unsigned char mine_explosion_far_wav[];
extern const unsigned mine_explosion_far_wav_size;
extern const unsigned char mine_explosion_near_wav[];
extern const unsigned mine_explosion_near_wav_size;
extern const unsigned char shooting_far_wav[];
extern const unsigned shooting_far_wav_size;
extern const unsigned char shooting_near_wav[];
extern const unsigned shooting_near_wav_size;
extern const unsigned char shooting_self_wav[];
extern const unsigned shooting_self_wav_size;
extern const unsigned char shot_building_far_wav[];
extern const unsigned shot_building_far_wav_size;
extern const unsigned char shot_building_near_wav[];
extern const unsigned shot_building_near_wav_size;
extern const unsigned char shot_tree_far_wav[];
extern const unsigned shot_tree_far_wav_size;
extern const unsigned char shot_tree_near_wav[];
extern const unsigned shot_tree_near_wav_size;
extern const unsigned char tank_sinking_far_wav[];
extern const unsigned tank_sinking_far_wav_size;
extern const unsigned char tank_sinking_near_wav[];
extern const unsigned tank_sinking_near_wav_size;
#endif /* _SOUNDRC */
