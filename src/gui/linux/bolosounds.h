/*
 * $Id$
 *
 * Copyright (c) 1998-2008 John Morrison.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "../sounds/soundrc.h"

/*********************************************************
*Name:          BoloSounds
*Filename:      BoloSounds.h
*Author:        John Morrison
*Creation Date: 28/12/98
*Last Modified: 28/12/98
*Purpose:
*  Provides Macro Defines for accessing the Sounds in the
*  BoloSounds.dll File.
*
* Notes: The DLL contains Resources all of type "WAV"
* with String Identifiers as follows.
*********************************************************/

#ifndef _BOLOSOUNDS_H
#define _BOLOSOUNDS_H

#define IDW_BIG_EXPLOSION_FAR big_explosion_far_wav
#define IDW_BIG_EXPLOSION_NEAR big_explosion_near_wav
#define IDW_BUBBLES bubbles_wav
#define IDW_FARMING_TREE_FAR farming_tree_far_wav
#define IDW_FARMING_TREE_NEAR farming_tree_near_wav
#define IDW_HIT_TANK_FAR hit_tank_far_wav
#define IDW_HIT_TANK_NEAR hit_tank_near_wav
#define IDW_HIT_TANK_SELF hit_tank_self_wav
#define IDW_MAN_BUILDING_FAR man_building_far_wav
#define IDW_MAN_BUILDING_NEAR man_building_near_wav
#define IDW_MAN_DYING_FAR man_dying_far_wav
#define IDW_MAN_DYING_NEAR man_dying_near_wav
#define IDW_MAN_LAYING_MINE_NEAR man_lay_mine_near_wav
#define IDW_MINE_EXPLOSION_FAR mine_explosion_far_wav
#define IDW_MINE_EXPLOSION_NEAR mine_explosion_near_wav
#define IDW_SHOOTING_FAR shooting_far_wav
#define IDW_SHOOTING_NEAR shooting_near_wav
#define IDW_SHOOTING_SELF shooting_self_wav
#define IDW_SHOT_BUILDING_FAR shot_building_far_wav
#define IDW_SHOT_BUILDING_NEAR shot_building_near_wav
#define IDW_SHOT_TREE_FAR shot_tree_far_wav
#define IDW_SHOT_TREE_NEAR shot_tree_near_wav
#define IDW_TANK_SINKING_FAR tank_sinking_far_wav
#define IDW_TANK_SINKING_NEAR tank_sinking_near_wav

#endif /* _BOLOSOUNDS_H */

